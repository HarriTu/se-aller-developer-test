<?php
/**
 * Plugin Name: Wordpress API widget
 * Description: Fetches articles from the article API.
 * Version: 0.0.1
 * Author: 
 */

define('WORDPRESS_API_WIDGET_PATH', plugin_dir_path(__FILE__));
define('WORDPRESS_API_WIDGET_URI', plugins_url('', __FILE__));